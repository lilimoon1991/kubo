-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-05-2021 a las 07:10:23
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kubo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baja_bien`
--

CREATE TABLE `baja_bien` (
  `Id_Baja` int(11) NOT NULL,
  `Id_Inventario` int(11) NOT NULL,
  `Id_Entrega` int(11) NOT NULL,
  `Fecha_Baja` date NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Motivo` enum('Robo','Perdida','Deterioro','Donacion') NOT NULL,
  `Contenido_Acta` text NOT NULL,
  `Etiqueta` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `baja_bien`
--

INSERT INTO `baja_bien` (`Id_Baja`, `Id_Inventario`, `Id_Entrega`, `Fecha_Baja`, `Cantidad`, `Motivo`, `Contenido_Acta`, `Etiqueta`) VALUES
(1, 2, 1, '2021-05-13', 1, 'Perdida', 'El día de hoy nos reunimos para dar de baja a los siguientes artículos  ', 'E002 '),
(2, 1, 2, '2021-05-10', 1, 'Deterioro', 'El día de hoy nos reunimos para dar de baja a los siguientes artículos  ', 'E001'),
(3, 3, 3, '2021-05-08', 1, 'Donacion', 'El día de hoy nos reunimos para dar de baja a los siguientes artículos  ', 'E003');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_cce`
--

CREATE TABLE `codigo_cce` (
  `Id_Codigo_CCE` int(11) NOT NULL,
  `Nombre_Producto` varchar(70) NOT NULL,
  `Codigo_Segmento` int(11) NOT NULL,
  `Nombre_Segmento` varchar(70) NOT NULL,
  `Codigo_Familia` int(11) NOT NULL,
  `Nombre_Familia` varchar(70) NOT NULL,
  `Codigo_Clase` int(11) NOT NULL,
  `Nombre_Clase` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `codigo_cce`
--

INSERT INTO `codigo_cce` (`Id_Codigo_CCE`, `Nombre_Producto`, `Codigo_Segmento`, `Nombre_Segmento`, `Codigo_Familia`, `Nombre_Familia`, `Codigo_Clase`, `Nombre_Clase`) VALUES
(11141601, 'Computador Portátil', 11, 'Equipos Electrónicos ', 1114, 'Equipos Electrónicos de oficina ', 111416, 'Computadores'),
(11141605, 'Celular 4G', 11, 'Equipos Electrónicos ', 1114, 'Equipos Electrónicos Móviles', 111416, 'Celulares '),
(11141701, 'Impresora ', 11, 'Equipos Electrónicos ', 1114, 'Equipos Electrónicos de oficina ', 111416, 'Impresoras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrega_inventario`
--

CREATE TABLE `entrega_inventario` (
  `Id_Entrega_Inventario` int(11) NOT NULL,
  `Fecha_Entrega_Servicio` date NOT NULL,
  `Bien_Activo` enum('Si','No') NOT NULL,
  `Etiqueta` varchar(11) DEFAULT NULL,
  `Cantidad` int(11) NOT NULL,
  `Nueva_Ubicacion` enum('Sede_Principal','Sede_Soapaga_Abejon','Sede_Boca_de_Monte','Sede_Guacal','Sede_La_Union_San_Jose','Sede_Tutaza','Sede_Llano_de_Miguel') NOT NULL,
  `Id_Responsable` int(11) NOT NULL,
  `Id_Inventario` int(11) NOT NULL,
  `Id_Codigo_CCE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entrega_inventario`
--

INSERT INTO `entrega_inventario` (`Id_Entrega_Inventario`, `Fecha_Entrega_Servicio`, `Bien_Activo`, `Etiqueta`, `Cantidad`, `Nueva_Ubicacion`, `Id_Responsable`, `Id_Inventario`, `Id_Codigo_CCE`) VALUES
(1, '2021-04-15', 'Si', 'E001', 1, 'Sede_Soapaga_Abejon', 1, 1, 11141605),
(2, '2021-04-30', 'Si', 'E002 ', 1, 'Sede_Guacal', 3, 2, 11141701),
(3, '2021-05-04', 'Si', 'E003', 1, 'Sede_Principal', 2, 3, 11141601);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `Id_Inventario` int(11) NOT NULL,
  `Tipo_Bien` enum('Bien_Devolutivo','Elemento_Consumible') NOT NULL,
  `Tipo_Ingreso` enum('Compra','Donacion') NOT NULL,
  `Fecha_Compra` date NOT NULL,
  `Fecha_Ingreso` date NOT NULL,
  `Id_Proveedor` int(11) NOT NULL,
  `Id_Codigo_CCE` int(11) NOT NULL,
  `Nombre_Articulo` varchar(80) NOT NULL,
  `Marca` varchar(50) DEFAULT NULL,
  `Serie` varchar(50) DEFAULT NULL,
  `Objeto_Contrato` text NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Descripcion` text NOT NULL,
  `Clasificacion` enum('Menor_Cuantia','Mayor_Cuantia') NOT NULL,
  `Conservacion` enum('Bueno','Regular','Malo') NOT NULL,
  `Ubicación` enum('Sede_Principal','Sede_Soapaga_Abejon','Sede_Boca_de_Monte','Sede_Guacal','Sede_La_Union_San_Jose','Sede_Tutaza','Sede_Llano_de_Miguel') NOT NULL,
  `Vida_Util(Meses)` enum('12','24','48','72','96') DEFAULT NULL,
  `Valor_Unitario` decimal(12,0) NOT NULL,
  `IVA` enum('Si','No') NOT NULL,
  `Porcentaje_Iva` enum('0','19') DEFAULT NULL,
  `Valor_IVA` decimal(12,0) DEFAULT NULL,
  `Valor_Total` decimal(12,0) NOT NULL,
  `Depresiable` enum('Si','NO') NOT NULL,
  `Sumatoria_Valor_Total` decimal(12,0) NOT NULL,
  `Sumatoria_Valor_Total_IVA` decimal(12,0) DEFAULT NULL,
  `Total` decimal(12,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`Id_Inventario`, `Tipo_Bien`, `Tipo_Ingreso`, `Fecha_Compra`, `Fecha_Ingreso`, `Id_Proveedor`, `Id_Codigo_CCE`, `Nombre_Articulo`, `Marca`, `Serie`, `Objeto_Contrato`, `Cantidad`, `Descripcion`, `Clasificacion`, `Conservacion`, `Ubicación`, `Vida_Util(Meses)`, `Valor_Unitario`, `IVA`, `Porcentaje_Iva`, `Valor_IVA`, `Valor_Total`, `Depresiable`, `Sumatoria_Valor_Total`, `Sumatoria_Valor_Total_IVA`, `Total`) VALUES
(1, 'Bien_Devolutivo', 'Compra', '2021-04-01', '2021-04-04', 1, 11141605, 'Celular Samsung Galaxy A70 4G ', 'Samsung', '0112D33R', 'CONT-SM-001-2021 Suministro de equipos electrónicos para funcionamiento en la institución IETSA', 2, 'Color Morado \r\nPulgadas 2 Pantalla\r\nMemoria ROM 16G\r\nMemoria RAM 4G\r\nProcesador Android 10 \r\n', 'Mayor_Cuantia', 'Bueno', 'Sede_Principal', '24', '1', 'Si', '19', '228', '1', 'Si', '2', '456', '2'),
(2, 'Bien_Devolutivo', 'Compra', '2021-04-01', '2021-04-04', 1, 11141601, 'Computar Portátil HP 15PUL', 'HP', '001124ER', 'CONT-SM-002-2021 Suministro de equipos electrónicos para funcionamiento en la institución IETSA', 1, 'Color blanco\r\nRAM 4 GB\r\nProcesador Intel(R) Celeron(R) N4000 CPU @ 1.10GHz   1.10 GHz\r\nMemoria 1 tera', 'Mayor_Cuantia', 'Bueno', 'Sede_Principal', '48', '1', 'Si', '19', '285', '1', 'Si', '1', '285', '1'),
(3, 'Bien_Devolutivo', 'Compra', '2021-04-01', '2021-04-04', 1, 11141701, 'Impresora Multicolor Epson', 'Epson', '00RRE-123', 'CONT-SM-003-2021 Suministro de equipos electrónicos para funcionamiento en la institución IETSA', 4, 'Color Gris\r\nTamaño Mediano \r\nMulticolor\r\nLaser', 'Mayor_Cuantia', 'Bueno', 'Sede_Principal', '96', '1', 'Si', '19', '190', '1', 'Si', '4', '760', '4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `Id_Proveedor` int(11) NOT NULL,
  `Nombre_Apellidos` varchar(50) NOT NULL,
  `Cedula_Nit` char(20) NOT NULL,
  `Telefono` char(15) NOT NULL,
  `Correo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`Id_Proveedor`, `Nombre_Apellidos`, `Cedula_Nit`, `Telefono`, `Correo`) VALUES
(1, 'Sergio Ramirez', '1.040.765.890', '3208906368', 'sergio998@hotmail.com'),
(2, 'Diego Alegría', '2185798', '3116547834', 'diego_alegria@gmail.com'),
(3, 'Jorge Muñoz', '70876098', '3114569762', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE `responsable` (
  `Id_Responsable` int(11) NOT NULL,
  `Nombre_Apellidos` varchar(50) NOT NULL,
  `Cedula` char(20) NOT NULL,
  `Cargo` varchar(50) NOT NULL,
  `Celular` varchar(15) NOT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Firma_Beneficiario` varchar(50) NOT NULL,
  `Firma_Quien_Entrega` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `responsable`
--

INSERT INTO `responsable` (`Id_Responsable`, `Nombre_Apellidos`, `Cedula`, `Cargo`, `Celular`, `Correo`, `Firma_Beneficiario`, `Firma_Quien_Entrega`) VALUES
(1, 'Liliana Maldonado ', '1.058.492.326', 'Docente ', '3148896765', 'lilimaldonado@gmail.com', 'Liliana Maldonado ', 'Gloria Muñoz '),
(2, 'Leidy Tapias ', '1.040.744.634', 'Docente ', '3117890987', 'leidytapias@hotmail.es', 'Leidy Tapias ', 'Gloria Muñoz '),
(3, 'Andrea Restrepo ', '1.094.678.543', 'Secretaria', '3225768965', 'andrea_0987@gmail.com', 'Andrea Restrepo ', 'Gloria Muñoz ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traslado`
--

CREATE TABLE `traslado` (
  `Id_Traslado` int(11) NOT NULL,
  `Id_Responsable` int(11) NOT NULL,
  `Id_Entrega` int(11) NOT NULL,
  `Nueva_Ubicación` enum('Sede_Principal','Sede_Soapaga_Abejon','Sede_Boca_de_Monte','Sede_Guacal','Sede_La_Union_San_Jose','Sede_Tutaza','Sede_Llano_de_Miguel') NOT NULL,
  `Fecha_Traslado` date NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Bien_Activo` enum('Si','No') NOT NULL,
  `Etiqueta` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `traslado`
--

INSERT INTO `traslado` (`Id_Traslado`, `Id_Responsable`, `Id_Entrega`, `Nueva_Ubicación`, `Fecha_Traslado`, `Cantidad`, `Bien_Activo`, `Etiqueta`) VALUES
(1, 1, 1, 'Sede_Tutaza', '2021-03-17', 2, 'Si', 'E001'),
(2, 2, 2, 'Sede_Principal', '2021-05-03', 1, 'Si', 'E002'),
(3, 3, 3, 'Sede_La_Union_San_Jose', '2021-05-07', 1, 'Si', 'E003');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id_Usuario` int(11) NOT NULL,
  `Cedula` char(20) NOT NULL,
  `Contrasena` varchar(10) NOT NULL,
  `Nombre_Apellidos` varchar(50) NOT NULL,
  `Cargo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Cedula`, `Contrasena`, `Nombre_Apellidos`, `Cargo`) VALUES
(1, '1.988.097.876', 'Arte', 'Gloria Muñoz', 'Secretaria'),
(2, '70.987.098', 'Solar-0', 'Jorge Maldonado', 'Rector '),
(3, '1.069.987.098', 'Nelsy_ppp', 'Nelsy Pereira ', 'Secretaria Administrativa');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `baja_bien`
--
ALTER TABLE `baja_bien`
  ADD PRIMARY KEY (`Id_Baja`),
  ADD KEY `FK_Id_Inventario` (`Id_Inventario`),
  ADD KEY `FK_Id_Entrega` (`Id_Entrega`);

--
-- Indices de la tabla `codigo_cce`
--
ALTER TABLE `codigo_cce`
  ADD PRIMARY KEY (`Id_Codigo_CCE`);

--
-- Indices de la tabla `entrega_inventario`
--
ALTER TABLE `entrega_inventario`
  ADD PRIMARY KEY (`Id_Entrega_Inventario`),
  ADD KEY `FK_Id_Responsable` (`Id_Responsable`),
  ADD KEY `FK_Id_Inventario` (`Id_Inventario`),
  ADD KEY `FK_Id_Codigo_CCE` (`Id_Codigo_CCE`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`Id_Inventario`),
  ADD KEY `FK_Id_Proveedor` (`Id_Proveedor`),
  ADD KEY `FK_Id_Codigo_CCE` (`Id_Codigo_CCE`) USING BTREE;

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`Id_Proveedor`),
  ADD UNIQUE KEY `Cedula_Nit` (`Cedula_Nit`);

--
-- Indices de la tabla `responsable`
--
ALTER TABLE `responsable`
  ADD PRIMARY KEY (`Id_Responsable`),
  ADD UNIQUE KEY `Cedula` (`Cedula`);

--
-- Indices de la tabla `traslado`
--
ALTER TABLE `traslado`
  ADD PRIMARY KEY (`Id_Traslado`),
  ADD KEY `FK_Id_Entrega` (`Id_Entrega`),
  ADD KEY `FK_Id_Responsable` (`Id_Responsable`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id_Usuario`),
  ADD UNIQUE KEY `Cedula` (`Cedula`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `baja_bien`
--
ALTER TABLE `baja_bien`
  MODIFY `Id_Baja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `codigo_cce`
--
ALTER TABLE `codigo_cce`
  MODIFY `Id_Codigo_CCE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11141702;

--
-- AUTO_INCREMENT de la tabla `entrega_inventario`
--
ALTER TABLE `entrega_inventario`
  MODIFY `Id_Entrega_Inventario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `Id_Inventario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `Id_Proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `responsable`
--
ALTER TABLE `responsable`
  MODIFY `Id_Responsable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `traslado`
--
ALTER TABLE `traslado`
  MODIFY `Id_Traslado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id_Usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `baja_bien`
--
ALTER TABLE `baja_bien`
  ADD CONSTRAINT `FK_Id_Entrega` FOREIGN KEY (`Id_Entrega`) REFERENCES `entrega_inventario` (`Id_Entrega_Inventario`),
  ADD CONSTRAINT `FK_Id_Inventario` FOREIGN KEY (`Id_Inventario`) REFERENCES `inventario` (`Id_Inventario`);

--
-- Filtros para la tabla `entrega_inventario`
--
ALTER TABLE `entrega_inventario`
  ADD CONSTRAINT `FK_Id_Responsable` FOREIGN KEY (`Id_Responsable`) REFERENCES `responsable` (`Id_Responsable`),
  ADD CONSTRAINT `entrega_inventario_ibfk_1` FOREIGN KEY (`Id_Inventario`) REFERENCES `inventario` (`Id_Inventario`),
  ADD CONSTRAINT `entrega_inventario_ibfk_2` FOREIGN KEY (`Id_Codigo_CCE`) REFERENCES `codigo_cce` (`Id_Codigo_CCE`);

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `FK_Id_Codigo_CCE` FOREIGN KEY (`Id_Codigo_CCE`) REFERENCES `codigo_cce` (`Id_Codigo_CCE`),
  ADD CONSTRAINT `FK_Id_Proveedor` FOREIGN KEY (`Id_Proveedor`) REFERENCES `proveedor` (`Id_Proveedor`);

--
-- Filtros para la tabla `traslado`
--
ALTER TABLE `traslado`
  ADD CONSTRAINT `traslado_ibfk_1` FOREIGN KEY (`Id_Responsable`) REFERENCES `responsable` (`Id_Responsable`),
  ADD CONSTRAINT `traslado_ibfk_2` FOREIGN KEY (`Id_Entrega`) REFERENCES `entrega_inventario` (`Id_Entrega_Inventario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
